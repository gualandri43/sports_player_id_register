# Player ID Register

This project aims to provide a wide range of player information and ID mappings in use across various data sources. Aggregating player data, stats, projections, etc. for
analysis across different websites can be an arduous task. The mappings developed in this project aim to make that task much easier by relating the unique identifiers
for each player from each website or data source.

The player ID register is primarily maintained as JSON files. Those may be downloaded directly from the repository. Translations into other formats are
provided on the [documentation website](https://gualandri43.gitlab.io/sports_player_id_maps).

# Data Sources

The player data and mappings are built using a combination of sources. The `tools` directory of this project contains scripts that provide the ability to download either
external mappings or player data from external sources and match them up to build up the combined mappings. Where necessary, manual matching is used.

Besides the ID values themselves, basic player information is included, referred to as `meta_*` fields (names, teams, etc.). The master player list and player info
for each record comes from either [Pro Football Reference](https://www.pro-football-reference.com) or [Baseball Reference](https://www.baseball-reference.com). All
other info is added using this as a base.

Existing mapping information is provided by the following external sources, and is used to aggregate some of the mappings here:
- Chadwick Bureau Persons Register
- SportsDataIO API
- Sleeper App
- nflverse

# Licenses

This project is solely intended to aid in linking players across different services. The use of data specific to each source is subject to
the license terms of each service. If the mapping linkages were obtained from an external source, this is noted in the [documentation](https://gualandri43.gitlab.io/sports_player_id_maps). Those fields are generally free to use, but please refer to those sources for licensing information.

The mapping relationships that do not have a source noted are free for anyone to use for any purpose.

# ID Mapping Data Dictionary

See website for descriptions of each data field provided by the mapping in the [documentation](https://gualandri43.gitlab.io/sports_player_id_maps).
